//
//  LayoutConfig.swift
//  UmHelpIOS
//
//  Created by Luis Belo on 19/01/20.
//  Copyright © 2020 Luis Belo. All rights reserved.
//

import Foundation
import UIKit

struct LayoutConfig {
    
    static var mainLightColor = UIColor(red: 252/255, green: 252/255, blue: 252/255, alpha: 1)
    
    static let labelForTextField: UIFont = UIFont(name: "Avenir-Light", size: 14)!
    static let textFiedStandardFont: UIFont = UIFont(name: "Avenir-Medium", size: 18)!

    
    static let defaultButtonInsets: UIEdgeInsets = UIEdgeInsets(top: 8, left: 0, bottom: 8, right: 0)
    
}

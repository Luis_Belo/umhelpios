//
//  RegisterService.swift
//  UmHelpIOS
//
//  Created by sacdigital on 12/02/20.
//  Copyright © 2020 Luis Belo. All rights reserved.
//

import Foundation

class RegisterService: BaseService<RegisterRequestProtocol> {
    
    override func onRequestSuccess(data: Data, responseCode: Int) {
        do {
            let formattedJson = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
            let message = formattedJson["message"] as? String
            
            if let verifiedMessage = message {
                self.requester.onRegisterSuccess(message: verifiedMessage)
            } else {
                self.requester.onRegisterSuccess(message: "Usuário registrado com sucesso!")
            }
            
        } catch {
            
        }
    }
    
    override func onRequestFailed(errorMessage: String) {
        self.requester.onRequestFailed(message: errorMessage)
    }
    
}

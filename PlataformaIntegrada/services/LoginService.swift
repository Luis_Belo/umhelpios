//
//  LoginService.swift
//  UmHelpIOS
//
//  Created by sacdigital on 10/02/20.
//  Copyright © 2020 Luis Belo. All rights reserved.
//

import Foundation
import Alamofire


struct LoginResponse : Decodable{
    var user: User
    var auth: String
    var token: String
}

class LoginService: BaseService<LoginRequestProtocol> {
    
    override func onRequestSuccess(data: Data, responseCode: Int) {
        
    }
    
    override func onRequestFailed(errorMessage: String) {
        
    }
}

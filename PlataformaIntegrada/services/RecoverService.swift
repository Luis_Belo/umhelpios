//
//  RecoverService.swift
//  UmHelpIOS
//
//  Created by Luis Belo on 19/02/20.
//  Copyright © 2020 Luis Belo. All rights reserved.
//

import Foundation

class RecoverService: BaseService<RecoverRequestProtocol> {
    
    override func onRequestSuccess(data: Data, responseCode: Int) {
        
    }
    
    override func onRequestFailed(errorMessage: String) {
        
    }
    
}

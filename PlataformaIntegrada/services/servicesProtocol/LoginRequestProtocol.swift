//
//  LoginRequestProtocol.swift
//  UmHelpIOS
//
//  Created by sacdigital on 10/02/20.
//  Copyright © 2020 Luis Belo. All rights reserved.
//

import Foundation

protocol LoginRequestProtocol : BaseRequestProtocol{
    func onLoginSuccess()
}

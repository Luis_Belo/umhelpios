//
//  RecoverRequestProtocol.swift
//  UmHelpIOS
//
//  Created by Luis Belo on 19/02/20.
//  Copyright © 2020 Luis Belo. All rights reserved.
//

import Foundation
protocol RecoverRequestProtocol : BaseRequestProtocol {
    func onRecoverSuccess(message: String)
}

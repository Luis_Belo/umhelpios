//
//  RegisterRequestProtocol.swift
//  UmHelpIOS
//
//  Created by sacdigital on 12/02/20.
//  Copyright © 2020 Luis Belo. All rights reserved.
//

import Foundation

protocol RegisterRequestProtocol : BaseRequestProtocol{
    func onRegisterSuccess(message: String)
}

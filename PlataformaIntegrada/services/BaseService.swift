//
//  BaseService.swift
//  UmHelpIOS
//
//  Created by sacdigital on 10/02/20.
//  Copyright © 2020 Luis Belo. All rights reserved.
//

import Foundation
import Alamofire

class BaseService<T>{
    
    var requester: T
    
    init(requester: T){
        self.requester = requester
    }
    
    func doRequest(url: String, parameter: [String:String]? = [:], method: HTTPMethod, header: [String:String]){
        AF.request(url, method: .post, parameters: parameter, encoder: JSONParameterEncoder.default).response { (response) in
            
            if let verifiedData = response.data {
                
                if response.response!.statusCode >= 200 && response.response!.statusCode <= 299{
                    self.onRequestSuccess(data: verifiedData, responseCode: response.response!.statusCode)
                } else if response.error != nil{
                    self.onRequestFailed(errorMessage: "Estamos enfrentando uma instabilidade, tente novamente mais tarde!")
                } else {
                    do{
                        let json = try JSONSerialization.jsonObject(with: verifiedData, options: []) as! [String : Any]
                        self.onRequestFailed(errorMessage: json["message"] as! String)
                    } catch {
                        self.onRequestFailed(errorMessage: "Estamos enfrentando uma instabilidade, tente novamente mais tarde!")
                    }
                }
                
            } else {
                self.onRequestFailed(errorMessage: "Estamos enfrentando uma instabilidade, tente novamente mais tarde!")
            }
            
        }
    }
    
    func onRequestSuccess(data: Data, responseCode: Int){}
    func onRequestFailed(errorMessage: String){}
    
}

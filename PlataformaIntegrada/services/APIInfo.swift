//
//  APIInfo.swift
//  UmHelpIOS
//
//  Created by sacdigital on 12/02/20.
//  Copyright © 2020 Luis Belo. All rights reserved.
//

import Foundation

struct APIInfo {
    
    private static let BASE_URL = "http://157.245.11.122:3300"
    public static let LOGIN_ENDPOINT = "\(APIInfo.BASE_URL)/login"
    public static let REGISTER_ENDPOINT = "\(APIInfo.BASE_URL)/register"
    
}

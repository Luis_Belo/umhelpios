//
//  USer.swift
//  UmHelpIOS
//
//  Created by sacdigital on 12/02/20.
//  Copyright © 2020 Luis Belo. All rights reserved.
//

import Foundation


struct User : Decodable{
    var id: String
    var nome: String
    var email: String
    
    enum CodingKeys: String, CodingKey {
        case id = "_id"
        case nome
        case email
    }
}

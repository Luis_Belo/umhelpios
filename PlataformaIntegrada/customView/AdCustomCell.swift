//
//  AdCustomCell.swift
//  UmHelpIOS
//
//  Created by Luis Belo on 28/01/20.
//  Copyright © 2020 Luis Belo. All rights reserved.
//

import Foundation
import UIKit

class AdCustomCell: UITableViewCell {
    
    var simpleCard = SimpleCard(frame: .zero)
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupLayout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupLayout(){
        self.backgroundColor = UIColor.clear
        
        let customSeparator = UIView()
        customSeparator.backgroundColor = UIColor.gray
        customSeparator.translatesAutoresizingMaskIntoConstraints = false
        
        self.simpleCard.translatesAutoresizingMaskIntoConstraints = false
        
        self.contentView.addSubview(self.simpleCard)
        self.contentView.addSubview(customSeparator)
        
        self.simpleCard.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor).isActive = true
        self.simpleCard.topAnchor.constraint(equalTo: self.contentView.topAnchor).isActive = true
        self.simpleCard.bottomAnchor.constraint(equalTo: customSeparator.topAnchor).isActive = true
        self.simpleCard.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor).isActive = true
        
        customSeparator.heightAnchor.constraint(equalToConstant: 1).isActive = true
        customSeparator.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor).isActive = true
        customSeparator.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor).isActive = true
        customSeparator.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor).isActive = true
        
    }
}

class ServicesMapHeader : UITableViewHeaderFooterView{
    
    var titleLabel: UILabel = {
        let question = UILabel()
        question.numberOfLines = 2
        question.textAlignment = .center
        question.adjustsFontSizeToFitWidth = true
        question.adjustsFontForContentSizeCategory = true
        question.font = UIFont(name: "Avenir-Medium", size: 18)!
        question.text = "Veja aqui os profissionais que estão perto de você!"
        return question
    }()
    
    var mapButton : UIButton = {
        let showMapButton = UIButton()
        showMapButton.setTitle("VER NO MAPA", for: .normal)
        showMapButton.setTitleColor(UIColor.black, for: .normal)
        showMapButton.layer.borderWidth = 1
        showMapButton.layer.borderColor = UIColor.black.cgColor
        showMapButton.layer.cornerRadius = 5
        showMapButton.contentEdgeInsets = UIEdgeInsets(top: 8, left: 0, bottom: 8, right: 0)
        return showMapButton
    }()
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        setupLayout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupLayout(){
        
        let mainStack = UIStackView()
        mainStack.axis = .vertical
        mainStack.translatesAutoresizingMaskIntoConstraints = false
        mainStack.spacing = 10
        mainStack.addArrangedSubview(titleLabel)
        mainStack.addArrangedSubview(mapButton)
        
        self.contentView.addSubview(mainStack)
        self.contentView.backgroundColor = UIColor.clear
        
        mainStack.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor).isActive = true
        mainStack.topAnchor.constraint(greaterThanOrEqualTo: self.contentView.topAnchor).isActive = true
        mainStack.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor).isActive = true
        mainStack.bottomAnchor.constraint(lessThanOrEqualTo: self.contentView.bottomAnchor).isActive = true
        mainStack.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor).isActive = true
    }
}

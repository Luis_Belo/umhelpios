//
//  AddressDescriptionView.swift
//  UmHelpIOS
//
//  Created by Luis Belo on 30/03/20.
//  Copyright © 2020 Luis Belo. All rights reserved.
//

import Foundation
import UIKit

class AddressDescriptionView : UIView{
    
    var titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        label.adjustsFontSizeToFitWidth = true
        label.adjustsFontForContentSizeCategory = true
        label.minimumScaleFactor = 0.5
        label.font = UIFont(name: "Avenir-Light", size: 18)!
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    var subtitleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        label.adjustsFontSizeToFitWidth = true
        label.adjustsFontForContentSizeCategory = true
        label.minimumScaleFactor = 0.5
        label.font = UIFont(name: "Avenir-Light", size: 14)!
        label.translatesAutoresizingMaskIntoConstraints = false
        
        return label
    }()
    
    private var dividerView: UIView = {
        let divider = UIView()
        divider.backgroundColor = UIColor.systemGray2
        divider.translatesAutoresizingMaskIntoConstraints = false
        return divider
    }()
    
    var legendIcon: UIImageView = {
        let icon = UIImageView(image: UIImage(systemName: "house"))
        icon.tintColor = UIColor.black
        icon.contentMode = .right
        icon.translatesAutoresizingMaskIntoConstraints = false
        icon.setContentCompressionResistancePriority(.defaultLow, for: .vertical)
        return icon
    }()
    
    private var chevronRight: UIImageView = {
        let imageView = UIImageView(image: UIImage(systemName: "chevron.right"))
        imageView.tintColor = UIColor.black
        imageView.contentMode = .right
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.setContentCompressionResistancePriority(.defaultLow, for: .vertical)
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func setupView(){
        let infoStack = UIStackView()
        infoStack.axis = .vertical
        infoStack.translatesAutoresizingMaskIntoConstraints = false
        infoStack.addArrangedSubview(self.titleLabel)
        infoStack.addArrangedSubview(self.subtitleLabel)
        
        self.addSubview(infoStack)
        self.addSubview(self.legendIcon)
        self.addSubview(self.dividerView)
        self.addSubview(self.chevronRight)
        
        infoStack.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 8).isActive = true
        infoStack.topAnchor.constraint(equalTo: self.topAnchor, constant: 8).isActive = true
        infoStack.trailingAnchor.constraint(equalTo: self.legendIcon.leadingAnchor, constant: -8).isActive = true
        infoStack.bottomAnchor.constraint(equalTo: self.dividerView.topAnchor, constant: -8).isActive = true

        self.legendIcon.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.2).isActive = true
        self.legendIcon.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        self.legendIcon.topAnchor.constraint(greaterThanOrEqualTo: infoStack.topAnchor).isActive = true
        self.legendIcon.trailingAnchor.constraint(equalTo: self.chevronRight.leadingAnchor, constant: -8).isActive = true
        self.legendIcon.bottomAnchor.constraint(lessThanOrEqualTo: infoStack.bottomAnchor).isActive = true
        
        self.chevronRight.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.2).isActive = true
        self.chevronRight.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        self.chevronRight.topAnchor.constraint(greaterThanOrEqualTo: infoStack.topAnchor).isActive = true
        self.chevronRight.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -8).isActive = true
        self.chevronRight.bottomAnchor.constraint(lessThanOrEqualTo: infoStack.bottomAnchor).isActive = true
        
        self.dividerView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        self.dividerView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        self.dividerView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        self.dividerView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
    }
    
}

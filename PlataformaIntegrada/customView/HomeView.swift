//
//  HomeView.swift
//  UmHelpIOS
//
//  Created by Igor Fernandes Cordeiro on 24/01/20.
//  Copyright © 2020 Luis Belo. All rights reserved.
//

import UIKit


class HomeView : UIView {
    
    var collectionView : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.register(OptionCell.self, forCellWithReuseIdentifier: "options")
        cv.register(HomeSearchCustomCell.self, forCellWithReuseIdentifier: "search")
        cv.register(IntroductionCell.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "header")
        
        layout.minimumInteritemSpacing = 0
        layout.minimumInteritemSpacing = 0
        
        cv.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 20, right: 0)
        cv.showsVerticalScrollIndicator = false
        
        return cv
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupView()
    }
    
    
    func setupView(){
        self.addSubview(collectionView)
        
        collectionView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        collectionView.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor).isActive = true
        collectionView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: self.safeAreaLayoutGuide.bottomAnchor).isActive = true
        
        collectionView.backgroundColor = UIColor.clear
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    
}

struct CustomDataOptionCell {
    var img: UIImage
    var title: String
    var backgroundColor: UIColor
    
}

class OptionCell : UICollectionViewCell {
    
    var data: CustomDataOptionCell? {
        didSet {
            guard let data = data else { return }
            imageCell.image = data.img
            titleCell.text = data.title
            self.contentView.backgroundColor = data.backgroundColor
        }
    }
    
    let imageCell : UIImageView = {
        let iv = UIImageView()
        iv.image = #imageLiteral(resourceName: "iconPainter")
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    
    let titleCell : UILabel = {
        let txt = UILabel()
        txt.text = "Pintor"
        txt.numberOfLines = 2
        txt.font = UIFont(name: "Avenir-Medium", size: 18)!
        txt.textColor = UIColor.white
        txt.translatesAutoresizingMaskIntoConstraints = false
        txt.adjustsFontSizeToFitWidth = true
        txt.textAlignment = .center
        return txt
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let viewContainer = UIView()
        viewContainer.translatesAutoresizingMaskIntoConstraints = false
        viewContainer.addSubview(imageCell)
        viewContainer.addSubview(titleCell)
        
        contentView.addSubview(viewContainer)
        
        viewContainer.widthAnchor.constraint(equalTo: contentView.widthAnchor, multiplier: 0.7).isActive = true
        viewContainer.heightAnchor.constraint(equalTo: contentView.heightAnchor, multiplier: 0.7).isActive = true
        viewContainer.centerYAnchor.constraint(equalTo: contentView.centerYAnchor).isActive = true
        viewContainer.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
        
        imageCell.topAnchor.constraint(equalTo: viewContainer.topAnchor).isActive = true
        imageCell.leadingAnchor.constraint(equalTo: viewContainer.leadingAnchor).isActive = true
        imageCell.trailingAnchor.constraint(equalTo: viewContainer.trailingAnchor).isActive = true
        imageCell.heightAnchor.constraint(equalTo: viewContainer.heightAnchor, multiplier: 0.6).isActive = true
        
        titleCell.topAnchor.constraint(equalTo: imageCell.bottomAnchor).isActive = true
        titleCell.leadingAnchor.constraint(equalTo: viewContainer.leadingAnchor).isActive = true
        titleCell.trailingAnchor.constraint(equalTo: viewContainer.trailingAnchor).isActive = true
        titleCell.bottomAnchor.constraint(equalTo: viewContainer.bottomAnchor).isActive = true
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}



class IntroductionCell : UICollectionReusableView {
    
    let introduction : UILabel = {
        let intro = UILabel()
        intro.text = "Seja bem-vindo(a), FULANO(A)!"
        intro.translatesAutoresizingMaskIntoConstraints = false
        intro.font = UIFont(name: "Avenir-Light", size: 14)!
        intro.textAlignment = .center
        intro.setContentCompressionResistancePriority(.defaultHigh, for: .vertical)
        return intro
    }()
    
    let titleLabel : UILabel = {
        let title = UILabel()
        title.text = "Selecione algum item das categorias abaixo"
        title.font = UIFont(name: "Avenir-Black", size: 48)!
        title.numberOfLines = 2
        title.adjustsFontSizeToFitWidth = true
        title.adjustsFontForContentSizeCategory = true
        title.setContentHuggingPriority(.defaultLow, for: .vertical)
        title.minimumScaleFactor = 0.2
        title.textAlignment = .center
        return title
    }()
    
    let titleStack : UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupHeader()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupHeader(){
        self.titleStack.addArrangedSubview(introduction)
        self.titleStack.addArrangedSubview(titleLabel)
        self.addSubview(titleStack)
        
        self.titleStack.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 30).isActive = true
        self.titleStack.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        self.titleStack.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -30).isActive = true
        self.titleStack.topAnchor.constraint(greaterThanOrEqualTo: self.topAnchor).isActive = true
        self.titleStack.bottomAnchor.constraint(lessThanOrEqualTo: self.bottomAnchor).isActive = true
        
    }
    
}

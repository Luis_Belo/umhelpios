//
//  RegisterView.swift
//  UmHelpIOS
//
//  Created by Luis Belo on 22/01/20.
//  Copyright © 2020 Luis Belo. All rights reserved.
//

import Foundation
import UIKit

class RegisterView {
    
    var nameTextField: UITextField = {
        let name = UITextField()
        name.font = LayoutConfig.textFiedStandardFont
        name.attributedPlaceholder = NSAttributedString(string: "Nome Completo", attributes: [NSAttributedString.Key.foregroundColor: LayoutConfig.mainLightColor.withAlphaComponent(0.5)])
        name.backgroundColor = UIColor.clear
        name.keyboardType = .asciiCapable
        name.layer.borderColor = LayoutConfig.mainLightColor.cgColor
        name.layer.borderWidth = 1
        name.borderStyle = .roundedRect
        name.layer.cornerRadius = 5
        name.textColor = LayoutConfig.mainLightColor
        name.addDoneButton()
        return name
    }()
    
    var emailTextField: UITextField = {
        let email = UITextField()
        email.font = LayoutConfig.textFiedStandardFont
        email.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedString.Key.foregroundColor: LayoutConfig.mainLightColor.withAlphaComponent(0.5)])
        email.backgroundColor = UIColor.clear
        email.keyboardType = .emailAddress
        email.layer.borderColor = LayoutConfig.mainLightColor.cgColor
        email.layer.borderWidth = 1
        email.borderStyle = .roundedRect
        email.layer.cornerRadius = 5
        email.textColor = LayoutConfig.mainLightColor
        email.addDoneButton()
        return email
    }()
    
    var cpfTextField: UITextField = {
        let cpfText = UITextField()
        cpfText.font = LayoutConfig.textFiedStandardFont
        cpfText.attributedPlaceholder = NSAttributedString(string: "CPF", attributes: [NSAttributedString.Key.foregroundColor: LayoutConfig.mainLightColor.withAlphaComponent(0.5)])
        cpfText.backgroundColor = UIColor.clear
        cpfText.keyboardType = .numberPad
        cpfText.layer.borderColor = LayoutConfig.mainLightColor.cgColor
        cpfText.layer.borderWidth = 1
        cpfText.borderStyle = .roundedRect
        cpfText.layer.cornerRadius = 5
        cpfText.textColor = LayoutConfig.mainLightColor
        cpfText.addDoneButton()
        return cpfText
    }()
    
    var genderSelect: UISegmentedControl = {
        let segmented = UISegmentedControl(items: ["Masculino", "Feminino"])
        segmented.backgroundColor = LayoutConfig.mainLightColor
        segmented.selectedSegmentIndex = 0
        return segmented
    }()
    
    var passwordTextField: UITextField = {
        let password = UITextField()
        password.font = LayoutConfig.textFiedStandardFont
        password.backgroundColor = UIColor.clear
        password.attributedPlaceholder = NSAttributedString(string: "Senha", attributes: [NSAttributedString.Key.foregroundColor: LayoutConfig.mainLightColor.withAlphaComponent(0.5)])
        password.isSecureTextEntry = true
        password.layer.borderColor = LayoutConfig.mainLightColor.cgColor
        password.layer.borderWidth = 1
        password.layer.cornerRadius = 5
        password.textColor = LayoutConfig.mainLightColor
        password.borderStyle = .roundedRect
        password.addDoneButton()
        return password
    }()
    
    var confirmPassword: UITextField = {
        let password = UITextField()
        password.font = LayoutConfig.textFiedStandardFont
        password.backgroundColor = UIColor.clear
        password.attributedPlaceholder = NSAttributedString(string: "Confirmar senha", attributes: [NSAttributedString.Key.foregroundColor: LayoutConfig.mainLightColor.withAlphaComponent(0.5)])
        password.isSecureTextEntry = true
        password.layer.borderColor = LayoutConfig.mainLightColor.cgColor
        password.layer.borderWidth = 1
        password.layer.cornerRadius = 5
        password.textColor = LayoutConfig.mainLightColor
        password.borderStyle = .roundedRect
        password.addDoneButton()
        return password
    }()
    
    public var registerButton: UIButton = {
        let register = UIButton()
        register.titleLabel?.font = LayoutConfig.textFiedStandardFont
        register.setTitle("Cadastrar", for: .normal)
        register.setTitleColor(UIColor(red: 0, green: 0, blue: 0, alpha: 0.8), for: .normal)
        register.setTitleColor(UIColor(red: 0, green: 0, blue: 0, alpha: 0.4), for: .highlighted)
        register.backgroundColor = LayoutConfig.mainLightColor
        register.layer.cornerRadius = 5
        return register
    }()
    
    public var backButton: UIButton = {
        let backButton = UIButton()
        backButton.titleLabel?.font = LayoutConfig.textFiedStandardFont
        backButton.setTitle("Voltar para o Login", for: .normal)
        backButton.setTitleColor(LayoutConfig.mainLightColor, for: .normal)
        backButton.setTitleColor(LayoutConfig.mainLightColor.withAlphaComponent(0.4), for: .highlighted)
        backButton.backgroundColor = UIColor.clear
        return backButton
    }()
    
    public func getView() -> UIView{
        let credentialStack = UIStackView()
        credentialStack.axis = .vertical
        credentialStack.distribution = .equalSpacing
        credentialStack.spacing = 15
        
        let mainStack = UIStackView()
        mainStack.axis = .vertical
        mainStack.spacing = 30
        
        let buttonStack = UIStackView()
        buttonStack.axis = .vertical
        buttonStack.spacing = 15
        
        credentialStack.addArrangedSubview(nameTextField)
        credentialStack.addArrangedSubview(emailTextField)
        credentialStack.addArrangedSubview(cpfTextField)
        credentialStack.addArrangedSubview(passwordTextField)
        credentialStack.addArrangedSubview(confirmPassword)
        credentialStack.addArrangedSubview(genderSelect)
        mainStack.addArrangedSubview(credentialStack)
        mainStack.addArrangedSubview(buttonStack)
        
        buttonStack.addArrangedSubview(registerButton)
        buttonStack.addArrangedSubview(backButton)
        
        return mainStack
    }
}

//
//  SimpleTextField.swift
//  UmHelpIOS
//
//  Created by Luis Belo on 17/03/20.
//  Copyright © 2020 Luis Belo. All rights reserved.
//

import Foundation
import UIKit

class SimpleTextField: UIView {
    
    let label: UILabel = {
        let titleLabel = UILabel()
        titleLabel.font = LayoutConfig.labelForTextField
        titleLabel.numberOfLines = 1
        return titleLabel
    }()
    
    let textField: UITextField = {
        let field = UITextField()
        field.font = LayoutConfig.textFiedStandardFont
        field.borderStyle = .roundedRect
        return field
    }()
    
    private let fieldStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = 8
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        self.setupView()
    }
    
    private func setupView(){
        self.fieldStack.addArrangedSubview(label)
        self.fieldStack.addArrangedSubview(textField)
        
        self.pinView(viewToPin: self.fieldStack)
    }
    
}

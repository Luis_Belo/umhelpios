//
//  RecoverPasswordView.swift
//  UmHelpIOS
//
//  Created by sacdigital on 05/02/20.
//  Copyright © 2020 Luis Belo. All rights reserved.
//

import Foundation
import UIKit

class RecoverPasswordView {
    private var emailTextField: UITextField = {
        let email = UITextField()
        email.font = LayoutConfig.textFiedStandardFont
        email.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedString.Key.foregroundColor: LayoutConfig.mainLightColor.withAlphaComponent(0.5)])
        email.backgroundColor = UIColor.clear
        email.keyboardType = .asciiCapable
        email.layer.borderColor = LayoutConfig.mainLightColor.cgColor
        email.layer.borderWidth = 1
        email.borderStyle = .roundedRect
        email.layer.cornerRadius = 5
        email.textColor = LayoutConfig.mainLightColor
        email.addDoneButton()
        return email
    }()
    
    public var recoverPassword: UIButton = {
        let register = UIButton()
        register.titleLabel?.font = LayoutConfig.textFiedStandardFont
        register.setTitle("Recuperar senha", for: .normal)
        register.setTitleColor(UIColor(red: 0, green: 0, blue: 0, alpha: 0.8), for: .normal)
        register.setTitleColor(UIColor(red: 0, green: 0, blue: 0, alpha: 0.4), for: .highlighted)
        register.backgroundColor = LayoutConfig.mainLightColor
        register.layer.cornerRadius = 5
        return register
    }()
    
    public var backButton: UIButton = {
        let backButton = UIButton()
        backButton.titleLabel?.font = LayoutConfig.textFiedStandardFont
        backButton.setTitle("Voltar para o Login", for: .normal)
        backButton.setTitleColor(LayoutConfig.mainLightColor, for: .normal)
        backButton.setTitleColor(LayoutConfig.mainLightColor.withAlphaComponent(0.4), for: .highlighted)
        backButton.backgroundColor = UIColor.clear
        return backButton
    }()
    
    public func getView() -> UIView{
        let credentialStack = UIStackView()
        credentialStack.axis = .vertical
        credentialStack.distribution = .equalSpacing
        credentialStack.spacing = 15
        
        let mainStack = UIStackView()
        mainStack.axis = .vertical
        mainStack.spacing = 30
        
        let buttonStack = UIStackView()
        buttonStack.axis = .vertical
        buttonStack.spacing = 15
        
        credentialStack.addArrangedSubview(emailTextField)
        mainStack.addArrangedSubview(credentialStack)
        mainStack.addArrangedSubview(buttonStack)
        
        buttonStack.addArrangedSubview(recoverPassword)
        buttonStack.addArrangedSubview(backButton)
        
        return mainStack
    }
}

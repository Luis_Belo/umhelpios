//
//  SimpleCard.swift
//  UmHelpIOS
//
//  Created by sacdigital on 04/02/20.
//  Copyright © 2020 Luis Belo. All rights reserved.
//

import Foundation
import UIKit

class SimpleCard: UIView {
    
    var userAvatar : UIImageView = {
        let image = UIImageView()
        image.translatesAutoresizingMaskIntoConstraints = false
        image.image = UIImage(named: "rosto_1")
        return image
    }()
    
    var userName: UILabel = {
        let name = UILabel()
        name.numberOfLines = 1
        name.adjustsFontSizeToFitWidth = true
        name.adjustsFontForContentSizeCategory = true
        name.font = UIFont(name: "Avenir-Medium", size: 18)!
        return name
    }()
    
    var userOccupation: UILabel = {
        let occupation = UILabel()
        occupation.numberOfLines = 1
        occupation.adjustsFontSizeToFitWidth = true
        occupation.font = UIFont(name: "Avenir-Light", size: 14)!
        return occupation
    }()
    
    var userRate: UIImageView = {
        let rateImage = UIImageView()
        rateImage.translatesAutoresizingMaskIntoConstraints = false
        rateImage.setContentCompressionResistancePriority(.defaultLow, for: .vertical)
        rateImage.image = UIImage(named: "five_star")
        rateImage.contentMode = .scaleAspectFit
        return rateImage
    }()
    
    var userAveragePrice: UILabel = {
        let average = UILabel()
        average.numberOfLines = 1
        average.adjustsFontSizeToFitWidth = true
        average.font = UIFont(name: "Avenir-Light", size: 14)!
        return average
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView(){
        let imageContainer = UIView()
        imageContainer.backgroundColor = UIColor.clear
        imageContainer.addSubview(userRate)
        imageContainer.translatesAutoresizingMaskIntoConstraints = false
        imageContainer.setContentCompressionResistancePriority(.defaultLow, for: .vertical)
        userRate.widthAnchor.constraint(equalTo: imageContainer.widthAnchor, multiplier: 0.3).isActive = true
        userRate.leadingAnchor.constraint(equalTo: imageContainer.leadingAnchor).isActive = true
        userRate.topAnchor.constraint(equalTo: imageContainer.topAnchor).isActive = true
        userRate.bottomAnchor.constraint(equalTo: imageContainer.bottomAnchor).isActive = true
        
        let infoStack = UIStackView()
        infoStack.axis = .vertical
        infoStack.distribution = .fillEqually
        infoStack.addArrangedSubview(userName)
        infoStack.addArrangedSubview(userOccupation)
        infoStack.addArrangedSubview(imageContainer)
        infoStack.addArrangedSubview(userAveragePrice)
        infoStack.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(userAvatar)
        self.addSubview(infoStack)
        
        userAvatar.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        userAvatar.topAnchor.constraint(greaterThanOrEqualTo: self.topAnchor).isActive = true
        userAvatar.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        userAvatar.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.25).isActive = true
        userAvatar.heightAnchor.constraint(equalTo: userAvatar.widthAnchor, multiplier: 1).isActive = true
        userAvatar.bottomAnchor.constraint(lessThanOrEqualTo: self.bottomAnchor).isActive = true
        
        infoStack.leadingAnchor.constraint(equalTo: userAvatar.trailingAnchor, constant: 15).isActive = true
        infoStack.topAnchor.constraint(greaterThanOrEqualTo: self.topAnchor).isActive = true
        infoStack.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        infoStack.bottomAnchor.constraint(lessThanOrEqualTo: self.bottomAnchor).isActive = true
        infoStack.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
    }
    
}

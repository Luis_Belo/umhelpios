//
//  SimpleTitleView.swift
//  UmHelpIOS
//
//  Created by Luis Belo on 13/03/20.
//  Copyright © 2020 Luis Belo. All rights reserved.
//

import Foundation
import UIKit


class SimpleDescriptionView: UIView {
    
    var titleLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        label.adjustsFontSizeToFitWidth = true
        label.adjustsFontForContentSizeCategory = true
        label.minimumScaleFactor = 0.5
        label.font = UIFont(name: "Avenir-Light", size: 18)!
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    private var dividerView: UIView = {
        let divider = UIView()
        divider.backgroundColor = UIColor.systemGray2
        divider.translatesAutoresizingMaskIntoConstraints = false
        return divider
    }()
    
    private var chevronRight: UIImageView = {
        let imageView = UIImageView(image: UIImage(systemName: "chevron.right"))
        imageView.tintColor = UIColor.black
        imageView.contentMode = .right
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.setContentCompressionResistancePriority(.defaultLow, for: .vertical)
        return imageView
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setupView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    private func setupView(){
        self.addSubview(self.titleLabel)
        self.addSubview(self.dividerView)
        self.addSubview(self.chevronRight)
        
        self.titleLabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 8).isActive = true
        self.titleLabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 8).isActive = true
        self.titleLabel.trailingAnchor.constraint(equalTo: self.chevronRight.leadingAnchor, constant: -8).isActive = true
        self.titleLabel.bottomAnchor.constraint(equalTo: self.dividerView.topAnchor, constant: -8).isActive = true
        
        self.chevronRight.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.2).isActive = true
        self.chevronRight.topAnchor.constraint(equalTo: self.titleLabel.topAnchor).isActive = true
        self.chevronRight.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -8).isActive = true
        self.chevronRight.bottomAnchor.constraint(equalTo: self.titleLabel.bottomAnchor).isActive = true
        
        self.dividerView.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
        self.dividerView.heightAnchor.constraint(equalToConstant: 1).isActive = true
        self.dividerView.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
        self.dividerView.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
        
    }
    
}

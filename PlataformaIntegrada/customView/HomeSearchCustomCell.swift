//
//  AdSearchCustomCell.swift
//  UmHelpIOS
//
//  Created by sacdigital on 29/01/20.
//  Copyright © 2020 Luis Belo. All rights reserved.
//

import Foundation
import UIKit

class HomeSearchCustomCell: UICollectionViewCell {
    
    var searchTextField : UITextField = {
        let searchTF = UITextField()
        searchTF.font = LayoutConfig.textFiedStandardFont
        searchTF.borderStyle = .roundedRect
        searchTF.layer.borderColor = UIColor.systemGray.cgColor
        searchTF.backgroundColor = UIColor.clear
        searchTF.layer.borderWidth = 1
        searchTF.layer.cornerRadius = 5
        searchTF.placeholder = "Buscar servico..."
        searchTF.keyboardType = .asciiCapable
        searchTF.translatesAutoresizingMaskIntoConstraints = false
        searchTF.addDoneButton()
        return searchTF
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView(){
        self.contentView.addSubview(searchTextField)
        
        searchTextField.leadingAnchor.constraint(equalTo: self.contentView.leadingAnchor).isActive = true
        searchTextField.centerYAnchor.constraint(equalTo: self.contentView.centerYAnchor).isActive = true
        searchTextField.trailingAnchor.constraint(equalTo: self.contentView.trailingAnchor).isActive = true
        searchTextField.topAnchor.constraint(greaterThanOrEqualTo: self.contentView.topAnchor).isActive = true
        searchTextField.bottomAnchor.constraint(lessThanOrEqualTo: self.contentView.bottomAnchor).isActive = true
        
    }
}

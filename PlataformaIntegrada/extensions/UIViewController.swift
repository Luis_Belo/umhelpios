//
//  UIViewController.swift
//  UmHelpIOS
//
//  Created by Igor Fernandes Cordeiro on 26/01/20.
//  Copyright © 2020 Luis Belo. All rights reserved.
//

import UIKit

extension UIViewController{
    func showNextViewController(_ to: UIViewController){
        self.navigationController?.show(to, sender: self)
    }
    
    func showPreviousViewController(){
        self.navigationController?.popViewController(animated: true)
    }
    
}

//
//  UITextFieldExtension.swift
//  UmHelpIOS
//
//  Created by Luis Belo on 20/01/20.
//  Copyright © 2020 Luis Belo. All rights reserved.
//

import Foundation
import UIKit
extension UITextField{
    func addDoneButton() {
        let keyboardToolbar = UIToolbar()
        keyboardToolbar.sizeToFit()
        let flexBarButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace,
                                            target: nil, action: nil)
        let doneBarButton = UIBarButtonItem(title: "Concluído", style: UIBarButtonItem.Style.done, target: self, action: #selector(UIView.endEditing(_:)))
        keyboardToolbar.items = [flexBarButton, doneBarButton]
        self.inputAccessoryView = keyboardToolbar
    }
}

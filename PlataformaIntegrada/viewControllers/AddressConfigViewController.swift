//
//  AddressConfigViewController.swift
//  UmHelpIOS
//
//  Created by Luis Belo on 30/03/20.
//  Copyright © 2020 Luis Belo. All rights reserved.
//

import Foundation
import UIKit

class AddressConfigViewController: UIViewController {
    
    private var mainStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = 25
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    private var addAddressButton: UIButton = {
        let button = UIButton(type: UIButton.ButtonType.roundedRect)
        button.setTitle("Adicionar", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private var removeAddressButton: UIButton = {
        let button = UIButton(type: UIButton.ButtonType.system)
        button.setTitle("Remover", for: .normal)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    let mainScroll: UIScrollView = {
        let scroll = UIScrollView()
        scroll.showsVerticalScrollIndicator = false
        scroll.translatesAutoresizingMaskIntoConstraints = false
        return scroll
    }()
       
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = LayoutConfig.mainLightColor
        self.title = "Adicionar/Editar Endereço"
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.prefersLargeTitles = true
        
        self.setupView()
    }
    
    private func setupView(){
        self.view.addSubview(self.mainScroll)
        self.mainScroll.pinView(viewToPin: self.mainStack)
        
        let cepTextField = SimpleTextField()
        cepTextField.label.text = "CEP"
        
        let infoStack = UIStackView()
        infoStack.axis = .vertical
        infoStack.spacing = 5
        infoStack.translatesAutoresizingMaskIntoConstraints = false
        
        let streetTextField = SimpleTextField()
        streetTextField.label.text = "Rua"
        
        let stateTextField = SimpleTextField()
        stateTextField.label.text = "Município"
        
        let complementTextField = SimpleTextField()
        complementTextField.label.text = "Complemento"
        
        infoStack.addArrangedSubview(streetTextField)
        infoStack.addArrangedSubview(stateTextField)
        infoStack.addArrangedSubview(complementTextField)
        
        self.mainStack.addArrangedSubview(cepTextField)
        self.mainStack.addArrangedSubview(infoStack)
        
        let buttonStack = UIStackView()
        buttonStack.axis = .vertical
        buttonStack.spacing = 10
        buttonStack.translatesAutoresizingMaskIntoConstraints = false
        buttonStack.addArrangedSubview(self.addAddressButton)
        buttonStack.addArrangedSubview(self.removeAddressButton)
        
        //constraints
        self.view.addSubview(buttonStack)
        
        buttonStack.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 15).isActive = true
        buttonStack.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -10).isActive = true
        buttonStack.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -15).isActive = true
        
        self.mainScroll.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        self.mainScroll.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
        self.mainScroll.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        self.mainScroll.bottomAnchor.constraint(equalTo: buttonStack.topAnchor, constant: -20).isActive = true
        self.mainScroll.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 1).isActive = true
        
        self.mainStack.widthAnchor.constraint(equalTo: self.mainScroll.widthAnchor, multiplier: 1).isActive = true
        
        self.mainStack.layoutMargins = UIEdgeInsets(top: 10, left: 15, bottom: 10, right: 15)
        self.mainStack.isLayoutMarginsRelativeArrangement = true
    }
    
}

//
//  HomeViewController.swift
//  UmHelpIOS
//
//  Created by Igor Fernandes Cordeiro on 24/01/20.
//  Copyright © 2020 Luis Belo. All rights reserved.
//

import UIKit


class HomeViewController : UIViewController {
    let myView = HomeView()
    let dataOptionCell = [
        CustomDataOptionCell(img: UIImage(named: "assistencia_icon")!, title: "Assistência Técnica", backgroundColor: UIColor(red: 66/255, green: 135/255, blue: 245/255, alpha: 1)),
        CustomDataOptionCell(img: UIImage(named: "fashion_icon")!, title: "Moda e Beleza", backgroundColor: UIColor(red: 48/255, green: 153/255, blue: 78/255, alpha: 1)),
        CustomDataOptionCell(img: UIImage(named: "housekeeper_icon")!, title: "Limpeza", backgroundColor: UIColor(red: 221/255, green: 88/255, blue: 12/255, alpha: 1)),
        CustomDataOptionCell(img: UIImage(named: "teacher_icon")!, title: "Aulas", backgroundColor: UIColor(red: 132/255, green: 12/255, blue: 221/255, alpha: 1)),
        CustomDataOptionCell(img: UIImage(named: "technology_icon")!, title: "Tecnologia", backgroundColor: UIColor.black),
        CustomDataOptionCell(img: UIImage(named: "workers_icon")!, title: "Construção e Reforma", backgroundColor: UIColor(red: 197/255, green: 29/255, blue: 29/255, alpha: 1))
    ]
    
    
    override func viewDidLoad() {
        self.view.backgroundColor = LayoutConfig.mainLightColor
        
        self.view.addSubview(myView)
        myView.translatesAutoresizingMaskIntoConstraints = false
        myView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 20).isActive = true
        myView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
        myView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -20).isActive = true
        myView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        
        myView.collectionView.delegate = self
        myView.collectionView.dataSource = self
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }
    
    /*override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.navigationBar.isHidden = true
    }*/
}

extension HomeViewController : UICollectionViewDelegateFlowLayout, UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if section == 0 {
            return 1
        }
        return dataOptionCell.count
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        if indexPath.section == 0{
            if kind == UICollectionView.elementKindSectionHeader{
                
                let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "header", for: indexPath) as! IntroductionCell
                header.introduction.text = "Bem vindo Igor"
                return header
            }
        }
        
        return UICollectionReusableView()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let widthCollectionGuide = collectionView.frame.width - (collectionView.collectionViewLayout as! UICollectionViewFlowLayout).minimumInteritemSpacing
        
        if indexPath.section == 0{
            let widthTwoItens = collectionView.frame.width - ((collectionView.frame.width / 2.6) * 2) // valor que restou entre as views horizontalmente
            let sideMarginWidth = (widthTwoItens / 2) - (widthTwoItens / 2) * 0.5
            return CGSize(width: collectionView.frame.width - (sideMarginWidth * 2), height: 70)
        }
        
        return CGSize(width: widthCollectionGuide/2.6, height: widthCollectionGuide/2.6)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        if section == 0{
            return UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20 )
        }
        
        /*
         Cálculo abaixo seve para eu criar margens laterais da collectionView para que os itens continuem do tamanho que desejamos
         e fiquem mais próximos uns dos outros, pois se deixamos a separação horizontal a cargo do collectionView ele vai por um espaçamento
         horizontal grande e não queremos isso.
         Logo eu faço um cálculo simples:
         - Primeiro eu pego a largura total da collectionView
         - Depois a largura de cada item e mutiplico por 2 (pois são dois em cada linha)
         - Subtraio a largura total da linha das somas das larguras de cada item. Isso me dará o valor que a collectionView coloca entre os itens para
         preencher horizontalmente a linha, mas queremos que ele fiquem juntos e não preenchendo horizontalmente e então precisamos colocar margem. Esta
         margem nada mais é do que este valor que a CV coloca entre eles dividido por 2 (right e left) subtraido de um valor que você deseja por entre eles.
         */
        let widthTwoItens = collectionView.frame.width - ((collectionView.frame.width / 2.6) * 2) // valor que restou entre as views horizontalmente
        let sideMarginWidth = (widthTwoItens / 2) - (widthTwoItens / 2) * 0.5
        
        (collectionView.collectionViewLayout as! UICollectionViewFlowLayout).minimumLineSpacing = (collectionView.frame.width - ((collectionView.frame.width / 2.6) * 2)) - (sideMarginWidth * 2)
        return UIEdgeInsets(top: 0, left: sideMarginWidth, bottom: 0, right: sideMarginWidth )
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        if section == 0 {
            return CGSize(width: collectionView.frame.width, height: 150)
        }
        return CGSize.zero
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if indexPath.section == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "search", for: indexPath) as! HomeSearchCustomCell
            return cell
        }
        
        let options = collectionView.dequeueReusableCell(withReuseIdentifier: "options", for: indexPath) as! OptionCell
        let optionData = self.dataOptionCell[indexPath.item]
        options.data = optionData
        options.layer.cornerRadius = 10
        options.clipsToBounds = true
        
        return options
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let nextVC = UINavigationController(rootViewController: SelectedServiceListViewController())
        nextVC.modalPresentationStyle = .fullScreen
        self.tabBarController?.present(nextVC, animated: true, completion: nil)
    }
    
    
}

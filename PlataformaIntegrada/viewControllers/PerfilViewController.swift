//
//  PerfilViewController.swift
//  UmHelpIOS
//
//  Created by sacdigital on 05/02/20.
//  Copyright © 2020 Luis Belo. All rights reserved.
//

import Foundation
import UIKit

class PerfilViewController : UIViewController{
    
    private var perfilImageView: UIImageView = {
        let image = UIImage(named: "rosto_1")
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    
    private var userNameLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        label.adjustsFontSizeToFitWidth = true
        label.adjustsFontForContentSizeCategory = true
        label.minimumScaleFactor = 0.5
        label.font = UIFont(name: "Avenir-Medium", size: 25)!
        label.text = "Marcelo Santana de Santos Amorim"
        
        return label
    }()

    private var adAmount: UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        label.adjustsFontSizeToFitWidth = true
        label.adjustsFontForContentSizeCategory = true
        label.minimumScaleFactor = 0.5
        label.font = UIFont(name: "Avenir-Light", size: 14)!
        label.text = "Total de anúncios: 14"
        
        return label
    }()
    
    private var profession: UILabel = {
        let label = UILabel()
        label.numberOfLines = 2
        label.adjustsFontSizeToFitWidth = true
        label.adjustsFontForContentSizeCategory = true
        label.minimumScaleFactor = 0.5
        label.font = UIFont(name: "Avenir-Light", size: 14)!
        label.text = "Profissão/Serviço: Não declarado"
        
        return label
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = LayoutConfig.mainLightColor
        self.setupView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        self.perfilImageView.layer.cornerRadius = self.perfilImageView.frame.size.height / 2
        self.perfilImageView.layer.borderWidth = 1
        self.perfilImageView.clipsToBounds = true
    }
    
    private func setupView(){
        
        let mainStack = UIStackView()
        mainStack.axis = .vertical
        mainStack.distribution = .fill
        mainStack.translatesAutoresizingMaskIntoConstraints = false
        mainStack.spacing = 40
        
        let itensStack = UIStackView()
        itensStack.axis = .vertical
        itensStack.distribution = .fill
        
        let userInfoStack = UIView()
        userInfoStack.translatesAutoresizingMaskIntoConstraints = false
        
        let userInfoDescriptionStack = UIStackView()
        userInfoDescriptionStack.translatesAutoresizingMaskIntoConstraints = false
        userInfoDescriptionStack.axis = .vertical
        userInfoDescriptionStack.addArrangedSubview(userNameLabel)
        userInfoDescriptionStack.addArrangedSubview(adAmount)
        userInfoDescriptionStack.addArrangedSubview(profession)
        
        userInfoStack.addSubview(perfilImageView)
        userInfoStack.addSubview(userInfoDescriptionStack)
        
        let personalData = SimpleDescriptionView()
        personalData.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(personalDataTapped)))
        personalData.titleLabel.text = "Dados Pessoais"
        
        let servicesData = SimpleDescriptionView()
        servicesData.titleLabel.text = "Dados dos Serviços Prestados"
        
        let plans = SimpleDescriptionView()
        plans.titleLabel.text = "Planos Adquiridos"
        
        let settings = SimpleDescriptionView()
        settings.titleLabel.text = "Configurações do App"
        
        mainStack.addArrangedSubview(userInfoStack)
        mainStack.addArrangedSubview(itensStack)
        
        itensStack.addArrangedSubview(personalData)
        itensStack.addArrangedSubview(servicesData)
        itensStack.addArrangedSubview(plans)
        itensStack.addArrangedSubview(settings)
        
        userInfoStack.heightAnchor.constraint(equalToConstant: self.view.frame.height * 0.2).isActive = true
        
        perfilImageView.leadingAnchor.constraint(equalTo: userInfoStack.leadingAnchor, constant: 8).isActive = true
        perfilImageView.topAnchor.constraint(equalTo: userInfoStack.topAnchor, constant: 15).isActive = true
        perfilImageView.bottomAnchor.constraint(equalTo: userInfoStack.bottomAnchor, constant: -15).isActive = true
        perfilImageView.widthAnchor.constraint(equalTo: perfilImageView.heightAnchor, multiplier: 1).isActive = true
        
        userInfoDescriptionStack.centerYAnchor.constraint(equalTo: userInfoStack.centerYAnchor).isActive = true
        userInfoDescriptionStack.leadingAnchor.constraint(equalTo: perfilImageView.trailingAnchor, constant: 8).isActive = true
        userInfoDescriptionStack.trailingAnchor.constraint(equalTo: userInfoStack.trailingAnchor, constant: -8).isActive = true
        
        self.view.addSubview(mainStack)
        
        mainStack.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 8).isActive = true
        mainStack.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 20).isActive = true
        mainStack.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -8).isActive = true
        mainStack.bottomAnchor.constraint(lessThanOrEqualTo: self.view.bottomAnchor, constant: -8).isActive = true
        
    }
    
    @objc private func personalDataTapped(){
        let personalVC = PersonalDataViewController()
        let personalNavigationVC = UINavigationController(rootViewController: personalVC)
        
        personalNavigationVC.modalPresentationStyle = .fullScreen
        self.present(personalNavigationVC, animated: true, completion: nil)
    }
}

//
//  SelectedServiceListViewController.swift
//  UmHelpIOS
//
//  Created by Luis Belo on 28/01/20.
//  Copyright © 2020 Luis Belo. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation

class SelectedServiceListViewController: UIViewController {
    
    private var adTableView : UITableView = {
        let table = UITableView()
        table.register(AdCustomCell.self, forCellReuseIdentifier: "adCell")
        table.register(ServicesMapHeader.self, forHeaderFooterViewReuseIdentifier: "mapHeader")
        table.translatesAutoresizingMaskIntoConstraints = false
        table.showsVerticalScrollIndicator = false
        table.separatorColor = UIColor.clear
        table.sectionIndexBackgroundColor = UIColor.clear
        return table
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setupLayout()
        self.title = "Anúncios"
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.prefersLargeTitles = true
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Filtrar", style: .plain, target: self, action: #selector(showFilterVC))
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .close, target: self, action: #selector(dismissServiceFlow))
    }
    
    private func setupLayout(){
        self.adTableView.delegate = self
        self.adTableView.dataSource = self
        self.view.backgroundColor = LayoutConfig.mainLightColor
        
        self.view.addSubview(adTableView)
        adTableView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 20).isActive = true
        adTableView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 10).isActive = true
        adTableView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -20).isActive = true
        adTableView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        adTableView.backgroundColor = UIColor.clear
        
    }
    
    @objc func dismissServiceFlow(){
        self.dismiss(animated: true, completion: nil)
    }
    
    @objc func showFilterVC(){
        showNextViewController(FilterViewController())
    }
    
    @objc func showWorkersMap(){
        let workersVC = WorkersMapViewController()
        self.navigationController?.pushViewController(workersVC, animated: true)
    }
}

extension SelectedServiceListViewController : UITableViewDelegate, UITableViewDataSource{
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableHeaderFooterView(withIdentifier: "mapHeader") as! ServicesMapHeader
        cell.tintColor = self.view.backgroundColor
        cell.mapButton.addTarget(self, action: #selector(showWorkersMap), for: .touchUpInside)
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 130
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 30
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "adCell") as! AdCustomCell
        cell.simpleCard.userAveragePrice.text = "Entre R$ 80 e 130 reais"
        cell.simpleCard.userName.text = "João Cordeiro da Silva Santos Pitangueira"
        cell.simpleCard.userOccupation.text = "Mestre de Obras"
        
        return cell
    }
    
    
}

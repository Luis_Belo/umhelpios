//
//  WorkersMapViewController.swift
//  UmHelpIOS
//
//  Created by sacdigital on 03/02/20.
//  Copyright © 2020 Luis Belo. All rights reserved.
//

import Foundation
import MapKit
import UIKit

class WorkersMapViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {
    
    var simpleCardContainer = UIView()
    var simpleCard = SimpleCard()
    var simpleCardTopAnchor: NSLayoutConstraint?
    var hideSimpleCardTopAnchorValue: CGFloat = -170
    
    var map : MKMapView = {
        var serviceMap = MKMapView()
        serviceMap.translatesAutoresizingMaskIntoConstraints = false
        let coordinate: CLLocationCoordinate2D = CLLocationCoordinate2D(latitude: -12.9402528, longitude: -38.4552007)
        let span = MKCoordinateSpan(latitudeDelta: 0.04, longitudeDelta: 0.04)
        let region = MKCoordinateRegion(center: coordinate, span: span)
        serviceMap.setRegion(region, animated: true)
        return serviceMap
    }()
    
    var effectView : UIVisualEffectView = {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.light)
        let effect = UIVisualEffectView(effect: blurEffect)
        let label = UILabel()
        label.text = "Atualizando localização"
        effect.contentView.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.centerYAnchor.constraint(equalTo: effect.centerYAnchor).isActive = true
        label.centerXAnchor.constraint(equalTo: effect.centerXAnchor).isActive = true
        return effect
    }()
    
    var locationService = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupLayout()
        
        self.viewRespectsSystemMinimumLayoutMargins = false
        
        locationService.delegate = self
        map.delegate = self
        self.setupSimpleCard()
        
        if CLLocationManager.locationServicesEnabled() {
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined, .restricted, .denied:
                locationService.requestWhenInUseAuthorization()
            case .authorizedAlways, .authorizedWhenInUse:
                locationService.requestLocation()
                showLoadingView(show: true)
            @unknown default:
                break
            }
        } else {
            showLocationNeedAlert()
        }
    }
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        let identifier = "Annotation"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
        
        if annotationView == nil {
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            annotationView!.canShowCallout = true
        } else {
            annotationView!.annotation = annotation
        }
        
        annotationView?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onPinTapped)))
        return annotationView
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        if status != CLAuthorizationStatus.authorizedWhenInUse && status != CLAuthorizationStatus.notDetermined {
            showLocationNeedAlert()
        } else if status == CLAuthorizationStatus.authorizedAlways || status == CLAuthorizationStatus.authorizedWhenInUse{
            locationService.requestLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print(error.localizedDescription)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        showLoadingView(show: false)
        let location = locations[locations.endIndex - 1]
        let span = MKCoordinateSpan(latitudeDelta: 0.04, longitudeDelta: 0.04)
        let region = MKCoordinateRegion(center: location.coordinate, span: span)
        map.setRegion(region, animated: true)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = location.coordinate
        annotation.title = "Funcionário"
        map.addAnnotation(annotation)
        
    }
    
    private func setupSimpleCard(){
        self.view.addSubview(self.simpleCardContainer)
        
        self.simpleCardContainer.addSubview(self.simpleCard)
        self.simpleCardContainer.translatesAutoresizingMaskIntoConstraints = false

        self.simpleCardContainer.backgroundColor = LayoutConfig.mainLightColor
        self.simpleCardContainer.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 20).isActive = true
        self.simpleCardTopAnchor = self.simpleCardContainer.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: self.hideSimpleCardTopAnchorValue)
        self.simpleCardTopAnchor?.isActive = true
        self.simpleCardContainer.heightAnchor.constraint(equalToConstant: 130).isActive = true
        self.simpleCardContainer.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -20).isActive = true
        
        self.simpleCard.translatesAutoresizingMaskIntoConstraints = false
        self.simpleCard.leadingAnchor.constraint(equalTo: self.simpleCardContainer.leadingAnchor, constant: 10).isActive = true
        self.simpleCard.topAnchor.constraint(equalTo: self.simpleCardContainer.topAnchor).isActive = true
        self.simpleCard.trailingAnchor.constraint(equalTo: self.simpleCardContainer.trailingAnchor, constant: -10).isActive = true
        self.simpleCard.bottomAnchor.constraint(equalTo: self.simpleCardContainer.bottomAnchor).isActive = true
        
        self.simpleCardContainer.alpha = 0
        self.simpleCardContainer.layer.cornerRadius = 5
        
        //valores teste
        self.simpleCard.userAveragePrice.text = "Entre R$ 80 e 130 reais"
        self.simpleCard.userName.text = "João Cordeiro da Silva Santos Pitangueira"
        self.simpleCard.userOccupation.text = "Mestre de Obras"
    }
    
    private func showWorkerInfo(_ show: Bool, completion: (() -> Void)?){
        
        func simpleCardIsVisible() -> Bool{
            if self.simpleCardTopAnchor!.constant > 0{
                return true
            }
            return false
        }
        
        if show {
            
            if simpleCardIsVisible() {
                self.simpleCardTopAnchor?.constant = -170
                UIView.animate(withDuration: 0.4, animations: {
                    self.simpleCardContainer.alpha = 0
                    self.view.layoutIfNeeded()
                }) { (completed) in
                    completion?()
                    self.simpleCardTopAnchor?.constant = 20
                    UIView.animate(withDuration: 0.4) {
                        self.simpleCardContainer.alpha = 1
                        self.view.layoutIfNeeded()
                    }
                }
                
            } else {
                
                self.simpleCardTopAnchor?.constant = 20
                UIView.animate(withDuration: 0.4) {
                    self.simpleCardContainer.alpha = 1
                    self.view.layoutIfNeeded()
                }

            }
        } else {
            
            if simpleCardIsVisible() {
                self.simpleCardTopAnchor?.constant = -170
                UIView.animate(withDuration: 0.4, animations: {
                    self.simpleCardContainer.alpha = 0
                    self.view.layoutIfNeeded()
                }) { (completed) in
                    completion?()
                }
                
            }
            completion?()
        }
        
    }
    
    private func showLocationNeedAlert(){
        let alertAction = UIAlertAction(title: "Ok", style: .cancel) { (actions) in
            self.navigationController?.popViewController(animated: true)
        }
        let alertController = UIAlertController(title: "Precisamos da sua localização", message: "Sua localização é necessária para mapearmos os profissionais próximos a você. Por favor habilite a localização para o umHelp nas suas configurações", preferredStyle: .alert)
        alertController.addAction(alertAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    private func setupLayout(){
        self.navigationController?.navigationBar.prefersLargeTitles = false
        self.title = "Selecione"
        
        self.view.backgroundColor = LayoutConfig.mainLightColor
        
        self.view.addSubview(map)
        self.map.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        self.map.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
        self.map.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        self.map.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        
        self.map.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onMapTapped)))
        
        effectView.isHidden = true
        effectView.frame = view.bounds
        effectView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.addSubview(effectView)
    }
    
    private func showLoadingView(show: Bool){
        self.effectView.isHidden = !show
    }
    
    @objc func onPinTapped(){
        showWorkerInfo(true, completion: nil)
    }
    
    @objc func onMapTapped(){
        showWorkerInfo(false, completion: nil)
    }
    
}

//
//  BaseViewController.swift
//  UmHelpIOS
//
//  Created by Luis Belo on 19/02/20.
//  Copyright © 2020 Luis Belo. All rights reserved.
//

import Foundation
import UIKit

class BaseViewController: UIViewController {
    
    private let loadingView : UIVisualEffectView = {
        let blurEffect = UIBlurEffect(style: UIBlurEffect.Style.dark)
        let effect = UIVisualEffectView(effect: blurEffect)
        let progress = UIActivityIndicatorView()
        let label = UILabel()
        let progressStack = UIStackView()
        progressStack.axis = .vertical
        
        progressStack.addArrangedSubview(progress)
        progressStack.addArrangedSubview(label)
        
        label.text = "Aguarde..."
        progress.startAnimating()
        
        effect.contentView.addSubview(progressStack)
        progressStack.translatesAutoresizingMaskIntoConstraints = false
        progressStack.centerYAnchor.constraint(equalTo: effect.centerYAnchor).isActive = true
        progressStack.centerXAnchor.constraint(equalTo: effect.centerXAnchor).isActive = true
        return effect
    }()
    
    
    func showLoadingAnimation(){
        loadingView.frame = view.bounds
        loadingView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.view.addSubview(loadingView)
    }
    
    func hideLoadingAnimation(){
        loadingView.removeFromSuperview()
    }
    
    func showSimpleAlert(title: String, message: String, style: UIAlertAction.Style){
        let alertAction = UIAlertAction(title: title, style: style, handler: nil)
        
        let alertController = UIAlertController(title: "Aviso!", message: message, preferredStyle: .actionSheet)
        alertController.addAction(alertAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
}

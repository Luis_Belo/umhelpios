//
//  StartViewController.swift
//  UmHelpIOS
//
//  Created by Luis Belo on 19/01/20.
//  Copyright © 2020 Luis Belo. All rights reserved.
//

import UIKit
import Foundation


class StartViewController : BaseViewController, RegisterRequestProtocol{
    
    private var backgroundImage: UIImageView  = {
        let image = UIImage(named: "back-main-\(Int.random(in: 1...4))")
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private var darkBackgroundImageEffect: UIView = {
        let backgroundEffectView = UIView()
        backgroundEffectView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8)
        backgroundEffectView.translatesAutoresizingMaskIntoConstraints = false
        return backgroundEffectView
    }()
    
    private var mainStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.alpha = 0
        return stack
    }()
    
    private var emailTextField: UITextField = {
        let email = UITextField()
        email.font = LayoutConfig.textFiedStandardFont
        email.attributedPlaceholder = NSAttributedString(string: "email", attributes: [NSAttributedString.Key.foregroundColor: LayoutConfig.mainLightColor.withAlphaComponent(0.5)])
        email.backgroundColor = UIColor.clear
        email.keyboardType = .asciiCapable
        email.layer.borderColor = LayoutConfig.mainLightColor.cgColor
        email.layer.borderWidth = 1
        email.borderStyle = .roundedRect
        email.layer.cornerRadius = 5
        email.textColor = LayoutConfig.mainLightColor
        email.addDoneButton()
        return email
    }()
    
    private var passwordTextField: UITextField = {
        let password = UITextField()
        password.font = LayoutConfig.textFiedStandardFont
        password.backgroundColor = UIColor.clear
        password.attributedPlaceholder = NSAttributedString(string: "senha", attributes: [NSAttributedString.Key.foregroundColor: LayoutConfig.mainLightColor.withAlphaComponent(0.5)])
        password.isSecureTextEntry = true
        password.layer.borderColor = LayoutConfig.mainLightColor.cgColor
        password.layer.borderWidth = 1
        password.layer.cornerRadius = 5
        password.textColor = LayoutConfig.mainLightColor
        password.borderStyle = .roundedRect
        password.addDoneButton()
        return password
    }()
    
    private var businessLogo: UIImageView = {
        let image = UIImage(named: "hand")
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    private var loginButton: UIButton = {
        let button = UIButton()
        button.titleLabel?.font = LayoutConfig.textFiedStandardFont
        button.setTitle("Entrar", for: .normal)
        button.setTitleColor(UIColor(red: 0, green: 0, blue: 0, alpha: 0.8), for: .normal)
        button.setTitleColor(UIColor(red: 0, green: 0, blue: 0, alpha: 0.4), for: .highlighted)
        button.backgroundColor = LayoutConfig.mainLightColor
        button.layer.cornerRadius = 5
        return button
    }()
    
    private var registerButton: UIButton = {
        let button = UIButton()
        button.titleLabel?.font = LayoutConfig.textFiedStandardFont
        button.setTitle("Cadastrar", for: .normal)
        button.setTitleColor(LayoutConfig.mainLightColor, for: .normal)
        button.setTitleColor(LayoutConfig.mainLightColor.withAlphaComponent(0.4), for: .highlighted)
        button.backgroundColor = UIColor.clear
        return button
    }()
    
    private var recoverButton: UIButton = {
        let button = UIButton()
        button.titleLabel?.font = LayoutConfig.textFiedStandardFont
        button.setTitle("Recuperar senha", for: .normal)
        button.setTitleColor(LayoutConfig.mainLightColor, for: .normal)
        button.setTitleColor(LayoutConfig.mainLightColor.withAlphaComponent(0.4), for: .highlighted)
        button.backgroundColor = UIColor.clear
        return button
    }()
    
    private var registerLayout = RegisterView()
    private var registerView: UIView?
    
    private var recoverLayout = RecoverPasswordView()
    private var recoverView: UIView?
    
    //Contraints register view
    private var visibleRegisterCenterXAnchor: NSLayoutConstraint?
    private var hideRegisterCenterXAnchor: NSLayoutConstraint?
    //Contraints register view
    
    //Contraints login view
    private var visibleLoginCenterXAnchor: NSLayoutConstraint?
    private var hideLoginCenterXAnchor: NSLayoutConstraint?
    //Contraints login view
    
    //Constraints recover view
    private var visibleRecoverCenterXAnchor: NSLayoutConstraint?
    private var hideRecoverCenterXAnchor: NSLayoutConstraint?
    //Contraints recover view
    
    private var defaultAnimatorTime = 0.4
    
    private var centerLogoConst: NSLayoutConstraint?
    private var topLogoConst: NSLayoutConstraint?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        startMockedAnimation()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
          return .lightContent
    }
    
    private func setupViews(){
        self.registerView = registerLayout.getView()
        self.registerView?.translatesAutoresizingMaskIntoConstraints = false
        
        self.recoverView = recoverLayout.getView()
        self.recoverView?.translatesAutoresizingMaskIntoConstraints = false
        
        self.view.pinView(viewToPin: self.backgroundImage)
        self.view.pinView(viewToPin: self.darkBackgroundImageEffect)
        self.view.addSubview(self.businessLogo)
        self.view.addSubview(self.mainStack)
        self.view.addSubview(self.registerView!)
        self.view.addSubview(self.recoverView!)
        
        
        self.businessLogo.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.4).isActive = true
        self.businessLogo.heightAnchor.constraint(equalTo: self.businessLogo.heightAnchor).isActive = true
        self.centerLogoConst = self.businessLogo.centerYAnchor.constraint(equalTo: self.view.centerYAnchor)
        self.centerLogoConst?.isActive = true
        self.businessLogo.centerXAnchor.constraint(equalTo: self.view.centerXAnchor).isActive = true
        self.topLogoConst = self.businessLogo.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 40)
        
        let emailCredentialStack = UIStackView()
        emailCredentialStack.axis = .vertical
    
        emailCredentialStack.addArrangedSubview(emailTextField)
        
        let passwordCredentialStack = UIStackView()
        passwordCredentialStack.axis = .vertical
        passwordCredentialStack.addArrangedSubview(passwordTextField)
        
        let mainCredentialStack = UIStackView()
        mainCredentialStack.axis = .vertical
        mainCredentialStack.addArrangedSubview(emailCredentialStack)
        mainCredentialStack.addArrangedSubview(passwordCredentialStack)
        mainCredentialStack.spacing = 15
        
        let buttonsStack = UIStackView()
        buttonsStack.axis = .vertical
        buttonsStack.spacing = 10
        buttonsStack.addArrangedSubview(loginButton)
        buttonsStack.addArrangedSubview(registerButton)
        buttonsStack.addArrangedSubview(recoverButton)
        
        self.mainStack.addArrangedSubview(mainCredentialStack)
        self.mainStack.addArrangedSubview(buttonsStack)
        self.mainStack.spacing = 30

        setupLoginViewContraints()
        setupRegisterViewContraints()
        setupRecoverViewConstraints()
        
        loginButton.addTarget(self, action: #selector(onLoginButtonPressed), for: .touchUpInside)
        registerButton.addTarget(self, action: #selector(onShowRegisterLayoutPressed), for: .touchUpInside)
        recoverButton.addTarget(self, action: #selector(onShowRecoverLayoutPressed), for: .touchUpInside)
        
        self.registerLayout.registerButton.addTarget(self, action: #selector(onRegisterButtonPressed), for: .touchUpInside)
        self.registerLayout.backButton.addTarget(self, action: #selector(onShowLoginLayoutPressed), for: .touchUpInside)
        
        self.recoverLayout.recoverPassword.addTarget(self, action: #selector(onRecoverPasswordPressed), for: .touchUpInside)
        self.recoverLayout.backButton.addTarget(self, action: #selector(onRecoverBackButtonPressed), for: .touchUpInside)
    }
    
    private func setupLoginViewContraints(){
        self.mainStack.topAnchor.constraint(equalTo: self.businessLogo.bottomAnchor, constant: 40).isActive = true
        self.mainStack.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.8).isActive = true
        
        self.visibleLoginCenterXAnchor = self.mainStack.centerXAnchor.constraint(equalTo: self.view.centerXAnchor)
        self.visibleLoginCenterXAnchor?.isActive = true
        
        self.hideLoginCenterXAnchor = self.mainStack.centerXAnchor.constraint(equalTo: self.view!.centerXAnchor, constant: -(self.view!.frame.width + 60))
        
        
        self.mainStack.bottomAnchor.constraint(lessThanOrEqualTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: 20).isActive = true
    }
    
    private func setupRegisterViewContraints(){
        self.registerView!.topAnchor.constraint(equalTo: self.businessLogo.bottomAnchor, constant: 40).isActive = true
        self.registerView!.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.8).isActive = true
        
        self.hideRegisterCenterXAnchor = self.registerView!.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: self.view!.frame.width + 60)
        self.hideRegisterCenterXAnchor?.isActive = true
        
        self.visibleRegisterCenterXAnchor = self.registerView!.centerXAnchor.constraint(equalTo: self.view!.centerXAnchor)
        
        self.registerView!.bottomAnchor.constraint(lessThanOrEqualTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: 20).isActive = true
    }
    
    private func setupRecoverViewConstraints(){
        self.recoverView!.topAnchor.constraint(equalTo: self.businessLogo.bottomAnchor, constant: 40).isActive = true
        self.recoverView!.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 0.8).isActive = true
        
        self.hideRecoverCenterXAnchor = self.recoverView!.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: -(self.view!.frame.width + 60))
        self.hideRecoverCenterXAnchor?.isActive = true
        
        self.visibleRecoverCenterXAnchor = self.recoverView!.centerXAnchor.constraint(equalTo: self.view!.centerXAnchor)
        
        self.recoverView!.bottomAnchor.constraint(lessThanOrEqualTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: 20).isActive = true
    }
    
    //Actions of change views
    private func showLoginLayout(fromRecover: Bool){
        
        if fromRecover{
            self.visibleLoginCenterXAnchor?.isActive = true
            self.hideLoginCenterXAnchor?.constant =  -(self.view!.frame.width + 60)
            self.hideLoginCenterXAnchor?.isActive = false
        } else {
            self.visibleLoginCenterXAnchor?.isActive = true
            self.hideLoginCenterXAnchor?.isActive = false
        }
        
    }
    
    private func hideLoginLayout(fromRecover: Bool){
        
        if fromRecover {
            self.visibleLoginCenterXAnchor?.isActive = false
            self.hideLoginCenterXAnchor?.constant = self.view!.frame.width + 60
            self.hideLoginCenterXAnchor?.isActive = true
        } else {
            self.visibleLoginCenterXAnchor?.isActive = false
            self.hideLoginCenterXAnchor?.isActive = true
        }
        
    }
    
    private func showRegisterLayout(completion: @escaping () -> Void){
        self.visibleRegisterCenterXAnchor?.isActive = true
        self.hideRegisterCenterXAnchor?.isActive = false

    }
    
    private func hideRegisterLayout(completion: @escaping () -> Void){
        self.visibleRegisterCenterXAnchor?.isActive = false
        self.hideRegisterCenterXAnchor?.isActive = true
        
    }
    
    private func showRecoverLayout(){
        self.visibleRecoverCenterXAnchor?.isActive = true
        self.hideRecoverCenterXAnchor?.isActive = false
    }
    
    private func hideRecoverLayout(){
        self.visibleRecoverCenterXAnchor?.isActive = false
        self.hideRecoverCenterXAnchor?.isActive = true
    }
    //Actions of change views
    
    //metodos dos botoes de navegar entre as views
    @objc func onShowRegisterLayoutPressed(){
        
        UIView.animateKeyframes(withDuration: 0.7, delay: 0, options: [], animations: {
            self.hideLoginLayout(fromRecover: false)
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.7) {
                self.view.layoutIfNeeded()
            }
            
            self.showRegisterLayout {}
            UIView.addKeyframe(withRelativeStartTime: 0.3, relativeDuration: 0.7) {
                self.view.layoutIfNeeded()
            }
        }, completion: nil)
        
    }
    
    @objc func onShowRecoverLayoutPressed(){
        UIView.animateKeyframes(withDuration: 0.7, delay: 0, options: [], animations: {
            self.hideLoginLayout(fromRecover: true)
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.7) {
                self.view.layoutIfNeeded()
            }
            
            self.showRecoverLayout()
            UIView.addKeyframe(withRelativeStartTime: 0.3, relativeDuration: 0.7) {
                self.view.layoutIfNeeded()
            }
        }, completion: nil)
    }
    
    @objc func onShowLoginLayoutPressed(){
        
        UIView.animateKeyframes(withDuration: 0.7, delay: 0, options: [], animations: {
            self.hideRegisterLayout{}
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.7) {
                self.view.layoutIfNeeded()
            }
            
            self.showLoginLayout(fromRecover: false)
            UIView.addKeyframe(withRelativeStartTime: 0.3, relativeDuration: 0.7) {
                self.view.layoutIfNeeded()
            }
        }, completion: nil)
        
    }
    
    @objc func onRecoverBackButtonPressed(){
        UIView.animateKeyframes(withDuration: 0.7, delay: 0, options: [], animations: {
            self.hideRecoverLayout()
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.7) {
                self.view.layoutIfNeeded()
            }
            
            self.showLoginLayout(fromRecover: true)
            UIView.addKeyframe(withRelativeStartTime: 0.3, relativeDuration: 0.7) {
                self.view.layoutIfNeeded()
            }
        }, completion: nil)
    }
    //metodos dos botoes de navegar entre as views
    
    private func startMockedAnimation(){
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.centerLogoConst?.isActive = false
            self.topLogoConst?.isActive = true
            UIView.animate(withDuration: 0.6, animations: {
                self.view.layoutIfNeeded()

            }) { (true) in
                UIView.animate(withDuration: 0.6) {
                    self.mainStack.alpha = 1
                }
            }
            
        }
    }
    
    @objc func onLoginButtonPressed(){
        
        let tabBarController = UITabBarController()
        
        let perfilVC = PerfilViewController()
        let homeVC = HomeViewController()
        let adFormVC = AdFormViewController()
        
        homeVC.tabBarItem = UITabBarItem(title: "Home", image: UIImage(systemName: "house"), tag: 0)
        perfilVC.tabBarItem = UITabBarItem(title: "Perfil", image: UIImage(systemName: "person.circle"), tag: 1)
        adFormVC.tabBarItem = UITabBarItem(title: "Novo Anúncio", image: UIImage(systemName: "plus.circle"), tag: 1)
        
        let tabControllers = [homeVC, perfilVC, adFormVC]
        tabBarController.viewControllers = tabControllers
        
        tabBarController.modalPresentationStyle = .fullScreen
        self.present(tabBarController, animated: true, completion: nil)
        
    }
    
    @objc func onRegisterButtonPressed(){

        let name = registerLayout.nameTextField.text
        let email = registerLayout.emailTextField.text
        let segmentedValue = registerLayout.genderSelect.selectedSegmentIndex == 0 ? "M" : "F"
        let senha = registerLayout.passwordTextField.text
        let cpf = registerLayout.cpfTextField.text
        let params = [
            "nome" : name!,
            "email" : email!,
            "segmentedValue" : segmentedValue,
            "senha" : senha!,
            "cpf" : cpf!
        ]
        
        let registerService = RegisterService(requester: self)
        registerService.doRequest(url: APIInfo.REGISTER_ENDPOINT, parameter: params, method: .post, header: [:])

    }
    
    @objc func onRecoverPasswordPressed(){
        
    }
    
    func onRegisterSuccess(message: String) {
        let alertAction = UIAlertAction(title: "Ok", style: .default) { (onPressed) in
            self.onShowLoginLayoutPressed()
        }
        
        let alertController = UIAlertController(title: "Registrado!", message: message, preferredStyle: .actionSheet)
        alertController.addAction(alertAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
    
    func onRequestFailed(message: String) {
        
    }
    
}

extension StartViewController {
    
    private func isEmailValid(email: String) -> Bool{
        do {
            let regex = try NSRegularExpression(pattern: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}", options: .caseInsensitive)
            return regex.firstMatch(in: email, options: NSRegularExpression.MatchingOptions(rawValue: 0), range: NSMakeRange(0, email.count)) != nil
        } catch {
            return false
        }
    }
    
    
}

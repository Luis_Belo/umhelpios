//
//  PersonalDataViewController.swift
//  UmHelpIOS
//
//  Created by Luis Belo on 17/03/20.
//  Copyright © 2020 Luis Belo. All rights reserved.
//

import Foundation
import UIKit

class PersonalDataViewController : UIViewController{
    
    let mainScroll: UIScrollView = {
        let scroll = UIScrollView()
        scroll.showsVerticalScrollIndicator = false
        scroll.translatesAutoresizingMaskIntoConstraints = false
        return scroll
    }()
    
    let mainStack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = 15
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = LayoutConfig.mainLightColor
        self.title = "Dados Pessoais"
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.prefersLargeTitles = true
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .close, target: self, action: #selector(dismissScreen))
        
        self.setupViews()
    }
    
    @objc private func dismissScreen(){
        self.dismiss(animated: true, completion: nil)
    }
    
    private func setupViews(){
        self.view.addSubview(self.mainScroll)
        
        let fullName = SimpleTextField()
        fullName.label.text = "Nome Completo:"
        
        let email = SimpleTextField()
        email.label.text = "Email:"
        
        let birth = SimpleTextField()
        birth.label.text = "Data de Nascimento:"
        
        let cpf = SimpleTextField()
        cpf.label.text = "CPF:"
        
        let gender = UISegmentedControl(items: ["Masculino", "Feminino"])
        gender.selectedSegmentIndex = 0
        
        let address = SimpleDescriptionView()
        address.titleLabel.text = "Endereço"
        address.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(onAddressTapped(_:))))
        
        let password = SimpleDescriptionView()
        password.titleLabel.text = "Senha"
        
        self.mainStack.addArrangedSubview(fullName)
        self.mainStack.addArrangedSubview(email)
        self.mainStack.addArrangedSubview(birth)
        self.mainStack.addArrangedSubview(cpf)
        self.mainStack.addArrangedSubview(gender)
        self.mainStack.addArrangedSubview(address)
        self.mainStack.addArrangedSubview(password)
        
        self.mainScroll.pinView(viewToPin: self.mainStack)
        
        self.mainScroll.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        self.mainScroll.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
        self.mainScroll.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        self.mainScroll.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        self.mainScroll.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 1).isActive = true
        
        self.mainStack.widthAnchor.constraint(equalTo: self.mainScroll.widthAnchor, multiplier: 1).isActive = true
        
        self.mainStack.layoutMargins = UIEdgeInsets(top: 10, left: 15, bottom: 10, right: 15)
        self.mainStack.isLayoutMarginsRelativeArrangement = true
    }
    
    @objc func onAddressTapped(_ : UITapGestureRecognizer){
        let addressNavigation = UINavigationController(rootViewController: AddressListViewController())
        addressNavigation.modalPresentationStyle = .fullScreen
        self.present(addressNavigation, animated: true, completion: nil)
    }
    
}

//
//  AddressListViewController.swift
//  UmHelpIOS
//
//  Created by Luis Belo on 30/03/20.
//  Copyright © 2020 Luis Belo. All rights reserved.
//

import Foundation
import UIKit

class AddressListViewController : UIViewController{
    
    var mainScroll: UIScrollView = {
        let scroll = UIScrollView()
        scroll.showsVerticalScrollIndicator = false
        scroll.translatesAutoresizingMaskIntoConstraints = false
        return scroll
    }()
    
    var mainStack: UIStackView = {
        let stack = UIStackView()
        stack.spacing = 5
        stack.axis = .vertical
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    var addButton: UIButton = {
        let button = UIButton()
        button.setTitle("ADICIONAR NOVO ENDEREÇO", for: .normal)
        button.setTitleColor(UIColor.black, for: .normal)
        button.layer.borderWidth = 1
        button.layer.borderColor = UIColor.black.cgColor
        button.layer.cornerRadius = 5
        button.contentEdgeInsets = UIEdgeInsets(top: 8, left: 0, bottom: 8, right: 0)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.backgroundColor = LayoutConfig.mainLightColor
        self.title = "Meus Endereços"
        self.navigationController?.navigationBar.isHidden = false
        self.navigationController?.navigationBar.prefersLargeTitles = true
        
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .close, target: self, action: #selector(dismissScreen))
        self.mockData()
        self.setupViews()
    }
    
    @objc private func dismissScreen(){
        self.dismiss(animated: true, completion: nil)
    }
    
    private func setupViews(){
        self.view.addSubview(self.mainScroll)
        self.view.addSubview(self.addButton)
        
        self.mainScroll.pinView(viewToPin: self.mainStack)
        
        self.mainScroll.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        self.mainScroll.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor).isActive = true
        self.mainScroll.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        self.mainScroll.bottomAnchor.constraint(equalTo: self.addButton.topAnchor, constant: -5).isActive = true
        self.mainScroll.widthAnchor.constraint(equalTo: self.view.widthAnchor, multiplier: 1).isActive = true
        
        self.mainStack.widthAnchor.constraint(equalTo: self.mainScroll.widthAnchor, multiplier: 1).isActive = true
        
        self.mainStack.layoutMargins = UIEdgeInsets(top: 10, left: 15, bottom: 10, right: 15)
        self.mainStack.isLayoutMarginsRelativeArrangement = true
        
        self.addButton.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 15).isActive = true
        self.addButton.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -20).isActive = true
        self.addButton.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -15).isActive = true
        
        self.addButton.addTarget(self, action: #selector(addAddressTapped(_:)), for: .touchUpInside)
    }
    
    
    @objc private func addAddressTapped(_ : UITapGestureRecognizer){
        self.navigationController?.pushViewController(AddressConfigViewController(), animated: true)
    }
    
    private func mockData(){
        let address1 = AddressDescriptionView()
        address1.titleLabel.text = "Casa"
        address1.subtitleLabel.text = "Rua Cyridião Durval, n32, CASA, Pernambués"
        
        let address2 = AddressDescriptionView()
        address2.titleLabel.text = "Trabalho"
        address2.subtitleLabel.text = "Av. Luís Viana Filho, 4ª, Avenida, 410 - CAB, Salvador - BA, 41745-002"
        address2.legendIcon.image = UIImage(systemName: "map")
        
        let address3 = AddressDescriptionView()
        address3.titleLabel.text = "Casa da Mulher"
        address3.subtitleLabel.text = "Rua professora Cléa Bittencourt, 45, São Caetano"
        address3.legendIcon.image = UIImage(systemName: "map")
        
        self.mainStack.addArrangedSubview(address1)
        self.mainStack.addArrangedSubview(address2)
        self.mainStack.addArrangedSubview(address3)
    }
    
}

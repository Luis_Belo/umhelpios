//
//  FilterViewController.swift
//  UmHelpIOS
//
//  Created by Igor Fernandes Cordeiro on 30/01/20.
//  Copyright © 2020 Luis Belo. All rights reserved.
//

import UIKit

class FilterViewController : UITableViewController {
    
    let cellId = "cellId"
    var filtrosPorSessao:[String:[String]] = [
        "Localização" : ["Próximos","Mesmo Bairro"],
        "Avaliação" : ["3 ou mais estrelas","Boas Avaliações"],
        "Valores" : ["Até R$ 500,00"]
    ]
//    var filtrosPorSessao = [
//        ["Localização", "Próximos", "Mesmo Bairro"],
//        ["Avaliação", "3 ou mais estrelas", "Boas avaliações"],
//        ["Avaliação", "3 ou mais estrelas", "Boas avaliações"],
//        ["Avaliação", "3 ou mais estrelas", "Boas avaliações"],
//        ["Avaliação", "3 ou mais estrelas", "Boas avaliações"],
//        ["Avaliação", "3 ou mais estrelas", "Boas avaliações"],
//        ["Avaliação", "3 ou mais estrelas", "Boas avaliações"]
//       // ["Valores", "Até R$ 500,00"]
//    ]
    
    /*
     let filtrosPorSessao : [String : [String]] = [
         "Local" : ["Localização", "Próximos", "Mesmo Bairro"],
         "Avaliar" : ["Avaliação", "3 ou mais estrelas", "Boas avaliações"],
         "Valor" : ["Valores", "Até R$ 500,00"]
     ]
     */
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title = "Filtros"
        navigationController?.navigationBar.prefersLargeTitles = true
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: cellId)
        
    }
    
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let label = UILabel()
        
        label.text = Array(filtrosPorSessao)[section].key
        label.backgroundColor = UIColor.lightGray
        
        return label
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        
        return filtrosPorSessao.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return Array(filtrosPorSessao)[section].value.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)
        //_ = filtrosPorSessao[indexPath.section].remove(at: 0)[indexPath.row]
        
        cell.textLabel?.text = Array(filtrosPorSessao)[indexPath.section].value[indexPath.row]
            //filtrosPorSessao[indexPath.section][indexPath.row]
        //        let ignorandoPrimeiro = filtrosPorSessao[indexPath.section][indexPath.dropFirst()]
        //        cell.textLabel?.text = filtrosPorSessao[indexPath.section]
        
        
        return cell
    }
    
    
    
}
